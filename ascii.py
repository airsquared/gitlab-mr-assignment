def toLower(letter: str) -> str:
    if len(letter) != 1:
        raise Exception('Only one character')
    if not isAlphabet(letter):
        raise Exception('Not a letter')
    ascii = ord(letter)
    if 65 <= ascii <= 90:
        return chr(ascii + 32)
    else:
        return letter


def toUpper(letter: str) -> str:
    if len(letter) != 1:
        raise Exception('Only one character')
    if not isAlphabet(letter):
        raise Exception('Not a letter')
    ascii = ord(letter)
    if 97 <= ascii <= 122:
        return chr(ascii - 32)
    else:
        return letter


def isAlphabet(character: str) -> bool:
    if len(character) != 1:
        raise Exception('Only one character')
    ascii = ord(character)
    return 65 <= ascii <= 90 or 97 <= ascii <= 122


def isDigit(character: str) -> bool:
    if len(character) != 1:
        raise Exception('Only one character')
    ascii = ord(character)
    return 48 <= ascii <= 57


def isSpecial(character: str) -> bool:
    if len(character) != 1:
        raise Exception('Only one character')
    return not isAlphabet(character) and not isDigit(character)


assert toUpper('g') == 'G'
assert toLower('R') == 'r'
assert isAlphabet('a')
assert not isAlphabet('2')
assert isDigit('0')
assert isDigit('7')
assert not isDigit('c')
assert isSpecial(';')
assert isSpecial('ह')
assert not isSpecial('k')
